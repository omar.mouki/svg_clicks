import 'dart:developer';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:touchable/touchable.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      color = Colors.green;
    });
  }

  Color color = Colors.red;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: CanvasTouchDetector(
        builder: (context) => CustomPaint(
          size: Size(500, (500 * 2.0884353741496597).toDouble()),
          painter: RPSCustomPainter(context, color: color),
        ),
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}

//Add this CustomPaint widget to the Widget Tree

//Copy this CustomPainter code to the Bottom of the File
class RPSCustomPainter extends CustomPainter {
  final Color color;
  final BuildContext context;
  RPSCustomPainter(
    this.context, {
    required this.color,
  });

  @override
  bool? hitTest(Offset position) {
    // TODO: implement hitTest
    return super.hitTest(position);
  }

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.4299000, size.height * 0.8648306);
    path_0.cubicTo(
        size.width * 0.4299000,
        size.height * 0.8562345,
        size.width * 0.4444517,
        size.height * 0.8492671,
        size.width * 0.4624020,
        size.height * 0.8492671);
    path_0.lineTo(size.width * 0.5879796, size.height * 0.8492671);
    path_0.cubicTo(
        size.width * 0.6059299,
        size.height * 0.8492671,
        size.width * 0.6204816,
        size.height * 0.8562345,
        size.width * 0.6204816,
        size.height * 0.8648306);
    path_0.cubicTo(
        size.width * 0.6204816,
        size.height * 0.8734235,
        size.width * 0.6059299,
        size.height * 0.8803941,
        size.width * 0.5879796,
        size.height * 0.8803941);
    path_0.lineTo(size.width * 0.4624020, size.height * 0.8803941);
    path_0.cubicTo(
        size.width * 0.4444517,
        size.height * 0.8803941,
        size.width * 0.4299000,
        size.height * 0.8734235,
        size.width * 0.4299000,
        size.height * 0.8648306);
    path_0.close();
    var myCanvas = TouchyCanvas(context, canvas);

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Colors.green.withOpacity(1.0);
    // new code
    myCanvas.drawPath(path_0, paint_0_fill, onTapDown: (details) {
      print('leg clicked');
    }, onPanDown: (tapdetail) {
      // print("orange circle swiped");
    });
    // canvas.drawPath(path_0, paint_0_fill); // old generated code

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.1079224, size.height * 0.5786482);
    path_1.cubicTo(
        size.width * 0.09687143,
        size.height * 0.5718730,
        size.width * 0.09937891,
        size.height * 0.5620945,
        size.width * 0.1135245,
        size.height * 0.5568013);
    path_1.lineTo(size.width * 0.2124803, size.height * 0.5197818);
    path_1.cubicTo(
        size.width * 0.2266259,
        size.height * 0.5144919,
        size.width * 0.2470517,
        size.height * 0.5156906,
        size.width * 0.2581027,
        size.height * 0.5224658);
    path_1.cubicTo(
        size.width * 0.2691544,
        size.height * 0.5292378,
        size.width * 0.2666463,
        size.height * 0.5390195,
        size.width * 0.2525014,
        size.height * 0.5443094);
    path_1.lineTo(size.width * 0.1535449, size.height * 0.5813290);
    path_1.cubicTo(
        size.width * 0.1394000,
        size.height * 0.5866221,
        size.width * 0.1189741,
        size.height * 0.5854202,
        size.width * 0.1079224,
        size.height * 0.5786482);
    path_1.close();

    Paint paint_1_fill = Paint()..style = PaintingStyle.fill;
    paint_1_fill.color = Color(0xffFF86D0).withOpacity(1.0);
    canvas.drawPath(path_1, paint_1_fill);

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.4782898, size.height * 0.3934625);
    path_2.lineTo(size.width * 0.5990456, size.height * 0.4121564);
    path_2.cubicTo(
        size.width * 0.6136993,
        size.height * 0.4144267,
        size.width * 0.6245803,
        size.height * 0.4203583,
        size.width * 0.6273905,
        size.height * 0.4276091);
    path_2.lineTo(size.width * 0.6421224, size.height * 0.4656124);
    path_2.cubicTo(
        size.width * 0.6463524,
        size.height * 0.4765212,
        size.width * 0.6313088,
        size.height * 0.4870098,
        size.width * 0.6085224,
        size.height * 0.4890358);
    path_2.cubicTo(
        size.width * 0.6035551,
        size.height * 0.4894756,
        size.width * 0.5984612,
        size.height * 0.4894853,
        size.width * 0.5934871,
        size.height * 0.4890586);
    path_2.lineTo(size.width * 0.4337741, size.height * 0.4754007);
    path_2.cubicTo(
        size.width * 0.4109599,
        size.height * 0.4734495,
        size.width * 0.3957680,
        size.height * 0.4630130,
        size.width * 0.3998422,
        size.height * 0.4520879);
    path_2.cubicTo(
        size.width * 0.4000776,
        size.height * 0.4514593,
        size.width * 0.4003755,
        size.height * 0.4508339,
        size.width * 0.4007354,
        size.height * 0.4502150);
    path_2.lineTo(size.width * 0.4257707, size.height * 0.4072964);
    path_2.cubicTo(
        size.width * 0.4320048,
        size.height * 0.3966091,
        size.width * 0.4551537,
        size.height * 0.3903648,
        size.width * 0.4774748,
        size.height * 0.3933485);
    path_2.lineTo(size.width * 0.4782898, size.height * 0.3934625);
    path_2.lineTo(size.width * 0.4782898, size.height * 0.3934625);
    path_2.close();

    Paint paint_2_fill = Paint()..style = PaintingStyle.fill;
    paint_2_fill.color = Color(0xff1CB0F6).withOpacity(1.0);
    canvas.drawPath(path_2, paint_2_fill);

    Path path_3 = Path();
    path_3.moveTo(size.width * 0.4860476, size.height * 0.4819121);
    path_3.cubicTo(
        size.width * 0.5964238,
        size.height * 0.4819121,
        size.width * 0.6859048,
        size.height * 0.5246091,
        size.width * 0.6859048,
        size.height * 0.5772769);
    path_3.cubicTo(
        size.width * 0.6859048,
        size.height * 0.5825472,
        size.width * 0.6850068,
        size.height * 0.5877134,
        size.width * 0.6832857,
        size.height * 0.5927492);
    path_3.lineTo(size.width * 0.6832857, size.height * 0.5926352);
    path_3.cubicTo(
        size.width * 0.6832857,
        size.height * 0.5940554,
        size.width * 0.6829728,
        size.height * 0.5954756,
        size.width * 0.6823469,
        size.height * 0.5968632);
    path_3.lineTo(size.width * 0.5826129, size.height * 0.8185863);
    path_3.cubicTo(
        size.width * 0.5786333,
        size.height * 0.8274332,
        size.width * 0.5622966,
        size.height * 0.8337492,
        size.width * 0.5433959,
        size.height * 0.8337492);
    path_3.lineTo(size.width * 0.4389714, size.height * 0.8337492);
    path_3.cubicTo(
        size.width * 0.4168156,
        size.height * 0.8337492,
        size.width * 0.3988551,
        size.height * 0.8251498,
        size.width * 0.3988551,
        size.height * 0.8145407);
    path_3.cubicTo(
        size.width * 0.3988551,
        size.height * 0.8131824,
        size.width * 0.3991565,
        size.height * 0.8118241,
        size.width * 0.3997544,
        size.height * 0.8104951);
    path_3.lineTo(size.width * 0.4474401, size.height * 0.7044886);
    path_3.cubicTo(
        size.width * 0.4043844,
        size.height * 0.7195472,
        size.width * 0.3437605,
        size.height * 0.7159316,
        size.width * 0.3098327,
        size.height * 0.6958697);
    path_3.lineTo(size.width * 0.2200449, size.height * 0.6427785);
    path_3.cubicTo(
        size.width * 0.1850415,
        size.height * 0.6220814,
        size.width * 0.1917068,
        size.height * 0.5917166,
        size.width * 0.2349327,
        size.height * 0.5749544);
    path_3.cubicTo(
        size.width * 0.2507150,
        size.height * 0.5688339,
        size.width * 0.2691891,
        size.height * 0.5653746,
        size.width * 0.2879898,
        size.height * 0.5644560);
    path_3.cubicTo(
        size.width * 0.3011075,
        size.height * 0.5178502,
        size.width * 0.3847823,
        size.height * 0.4819121,
        size.width * 0.4860476,
        size.height * 0.4819121);
    path_3.close();

    Paint paint_3_fill = Paint()..style = PaintingStyle.fill;
    paint_3_fill.color = Color(0xffFFB7E3).withOpacity(1.0);
    canvas.drawPath(path_3, paint_3_fill);

    Path path_4 = Path();
    path_4.moveTo(size.width * 0.4928163, size.height * 0.5812182);
    path_4.lineTo(size.width * 0.4937796, size.height * 0.5813062);
    path_4.lineTo(size.width * 0.5487122, size.height * 0.5878925);
    path_4.cubicTo(
        size.width * 0.5532088,
        size.height * 0.5884332,
        size.width * 0.5559408,
        size.height * 0.5906156,
        size.width * 0.5548143,
        size.height * 0.5927687);
    path_4.cubicTo(
        size.width * 0.5537687,
        size.height * 0.5947687,
        size.width * 0.5497639,
        size.height * 0.5960391,
        size.width * 0.5455973,
        size.height * 0.5957785);
    path_4.lineTo(size.width * 0.5446347, size.height * 0.5956906);
    path_4.lineTo(size.width * 0.5329510, size.height * 0.5942899);
    path_4.lineTo(size.width * 0.5338612, size.height * 0.5975603);
    path_4.cubicTo(
        size.width * 0.5341429,
        size.height * 0.5986189,
        size.width * 0.5344143,
        size.height * 0.5996678,
        size.width * 0.5346748,
        size.height * 0.6007068);
    path_4.lineTo(size.width * 0.5351830, size.height * 0.6027752);
    path_4.cubicTo(
        size.width * 0.5430027,
        size.height * 0.6353518,
        size.width * 0.5403728,
        size.height * 0.6593029,
        size.width * 0.5269599,
        size.height * 0.6747980);
    path_4.cubicTo(
        size.width * 0.5142122,
        size.height * 0.6895244,
        size.width * 0.4916646,
        size.height * 0.6997687,
        size.width * 0.4644850,
        size.height * 0.7081173);
    path_4.lineTo(size.width * 0.4624381, size.height * 0.7087427);
    path_4.cubicTo(
        size.width * 0.4617531,
        size.height * 0.7089479,
        size.width * 0.4610653,
        size.height * 0.7091531,
        size.width * 0.4603741,
        size.height * 0.7093583);
    path_4.lineTo(size.width * 0.4582939, size.height * 0.7099674);
    path_4.lineTo(size.width * 0.4582939, size.height * 0.7099674);
    path_4.lineTo(size.width * 0.4561980, size.height * 0.7105700);
    path_4.lineTo(size.width * 0.4561980, size.height * 0.7105700);
    path_4.lineTo(size.width * 0.4540864, size.height * 0.7111661);
    path_4.lineTo(size.width * 0.4540864, size.height * 0.7111661);
    path_4.lineTo(size.width * 0.4519592, size.height * 0.7117557);
    path_4.cubicTo(
        size.width * 0.4516041,
        size.height * 0.7118567,
        size.width * 0.4512476,
        size.height * 0.7119544,
        size.width * 0.4508905,
        size.height * 0.7120521);
    path_4.lineTo(size.width * 0.4487429, size.height * 0.7126319);
    path_4.cubicTo(
        size.width * 0.4483837,
        size.height * 0.7127296,
        size.width * 0.4480238,
        size.height * 0.7128274,
        size.width * 0.4476633,
        size.height * 0.7129218);
    path_4.lineTo(size.width * 0.4454946, size.height * 0.7134984);
    path_4.lineTo(size.width * 0.4454946, size.height * 0.7134984);
    path_4.lineTo(size.width * 0.4433129, size.height * 0.7140651);
    path_4.lineTo(size.width * 0.4433129, size.height * 0.7140651);
    path_4.lineTo(size.width * 0.4431068, size.height * 0.7141173);
    path_4.lineTo(size.width * 0.4474401, size.height * 0.7044853);
    path_4.cubicTo(
        size.width * 0.4486102,
        size.height * 0.7041694,
        size.width * 0.4497735,
        size.height * 0.7038502,
        size.width * 0.4509299,
        size.height * 0.7035244);
    path_4.lineTo(size.width * 0.4526585, size.height * 0.7030293);
    path_4.cubicTo(
        size.width * 0.4558177,
        size.height * 0.7021173,
        size.width * 0.4589211,
        size.height * 0.7011629,
        size.width * 0.4619558,
        size.height * 0.7001629);
    path_4.lineTo(size.width * 0.4636048, size.height * 0.6996124);
    path_4.cubicTo(
        size.width * 0.4838537,
        size.height * 0.6927752,
        size.width * 0.5009429,
        size.height * 0.6838632,
        size.width * 0.5114544,
        size.height * 0.6717199);
    path_4.cubicTo(
        size.width * 0.5235952,
        size.height * 0.6576938,
        size.width * 0.5260333,
        size.height * 0.6348599,
        size.width * 0.5184327,
        size.height * 0.6033876);
    path_4.lineTo(size.width * 0.5179170, size.height * 0.6012997);
    path_4.cubicTo(
        size.width * 0.5178286,
        size.height * 0.6009511,
        size.width * 0.5177388,
        size.height * 0.6005993,
        size.width * 0.5176483,
        size.height * 0.6002476);
    path_4.lineTo(size.width * 0.5170884, size.height * 0.5981238);
    path_4.cubicTo(
        size.width * 0.5169932,
        size.height * 0.5977655,
        size.width * 0.5168959,
        size.height * 0.5974104,
        size.width * 0.5167980,
        size.height * 0.5970489);
    path_4.lineTo(size.width * 0.5161952, size.height * 0.5948893);
    path_4.cubicTo(
        size.width * 0.5160918,
        size.height * 0.5945244,
        size.width * 0.5159878,
        size.height * 0.5941629,
        size.width * 0.5158823,
        size.height * 0.5937980);
    path_4.cubicTo(
        size.width * 0.5157299,
        size.height * 0.5932671,
        size.width * 0.5157993,
        size.height * 0.5927492,
        size.width * 0.5160565,
        size.height * 0.5922638);
    path_4.lineTo(size.width * 0.4897020, size.height * 0.5891010);
    path_4.cubicTo(
        size.width * 0.4852054,
        size.height * 0.5885635,
        size.width * 0.4824735,
        size.height * 0.5863811,
        size.width * 0.4835993,
        size.height * 0.5842280);
    path_4.cubicTo(
        size.width * 0.4846456,
        size.height * 0.5822280,
        size.width * 0.4886497,
        size.height * 0.5809577,
        size.width * 0.4928163,
        size.height * 0.5812182);
    path_4.close();
    path_4.moveTo(size.width * 0.2877143, size.height * 0.5653355);
    path_4.cubicTo(
        size.width * 0.2869619,
        size.height * 0.5911596,
        size.width * 0.2961816,
        size.height * 0.6110423,
        size.width * 0.3150816,
        size.height * 0.6250228);
    path_4.cubicTo(
        size.width * 0.3176000,
        size.height * 0.6268860,
        size.width * 0.3164878,
        size.height * 0.6293746,
        size.width * 0.3125966,
        size.height * 0.6305798);
    path_4.cubicTo(
        size.width * 0.3087054,
        size.height * 0.6317850,
        size.width * 0.3035088,
        size.height * 0.6312541,
        size.width * 0.3009905,
        size.height * 0.6293909);
    path_4.cubicTo(
        size.width * 0.2802612,
        size.height * 0.6140554,
        size.width * 0.2703286,
        size.height * 0.5929186,
        size.width * 0.2709109,
        size.height * 0.5660130);
    path_4.cubicTo(
        size.width * 0.2763844,
        size.height * 0.5652736,
        size.width * 0.2819517,
        size.height * 0.5647622,
        size.width * 0.2875490,
        size.height * 0.5644788);
    path_4.cubicTo(
        size.width * 0.2876653,
        size.height * 0.5647557,
        size.width * 0.2877224,
        size.height * 0.5650423,
        size.width * 0.2877143,
        size.height * 0.5653355);
    path_4.close();

    Paint paint_4_fill = Paint()..style = PaintingStyle.fill;
    paint_4_fill.color = Color(0xffFFA1DA).withOpacity(1.0);
    canvas.drawPath(path_4, paint_4_fill);

    Path path_5 = Path();
    path_5.moveTo(size.width * 0.4158299, size.height * 0.4716352);
    path_5.lineTo(size.width * 0.6254993, size.height * 0.4892248);
    path_5.lineTo(size.width * 0.6192551, size.height * 0.5061824);
    path_5.lineTo(size.width * 0.4107408, size.height * 0.4889088);
    path_5.lineTo(size.width * 0.4158299, size.height * 0.4716352);
    path_5.close();

    Paint paint_5_fill = Paint()..style = PaintingStyle.fill;
    paint_5_fill.color = Color(0xffFF86D0).withOpacity(1.0);
    canvas.drawPath(path_5, paint_5_fill);

    Path path_6 = Path();
    path_6.moveTo(size.width * 0.4813837, size.height * 0.4771336);
    path_6.lineTo(size.width * 0.5099878, size.height * 0.4795342);
    path_6.lineTo(size.width * 0.5002231, size.height * 0.5007036);
    path_6.lineTo(size.width * 0.4716197, size.height * 0.4982899);
    path_6.lineTo(size.width * 0.4813837, size.height * 0.4771336);
    path_6.close();

    Paint paint_6_fill = Paint()..style = PaintingStyle.fill;
    paint_6_fill.color = Color(0xffFFB7E3).withOpacity(1.0);
    canvas.drawPath(path_6, paint_6_fill);

    Path path_7 = Path();
    path_7.moveTo(size.width * 0.5807823, size.height * 0.4854723);
    path_7.lineTo(size.width * 0.6093857, size.height * 0.4878860);
    path_7.lineTo(size.width * 0.5997395, size.height * 0.5087427);
    path_7.lineTo(size.width * 0.5711361, size.height * 0.5063257);
    path_7.lineTo(size.width * 0.5807823, size.height * 0.4854723);
    path_7.close();

    Paint paint_7_fill = Paint()..style = PaintingStyle.fill;
    paint_7_fill.color = Color(0xffFFB7E3).withOpacity(1.0);
    canvas.drawPath(path_7, paint_7_fill);

    Path path_8 = Path();
    path_8.moveTo(size.width * 0.6322857, size.height * 0.3866840);
    path_8.lineTo(size.width * 0.8544762, size.height * 0.4233160);
    path_8.cubicTo(
        size.width * 0.8851497,
        size.height * 0.4283746,
        size.width * 0.9014558,
        size.height * 0.4443811,
        size.width * 0.8908980,
        size.height * 0.4590717);
    path_8.cubicTo(
        size.width * 0.8803333,
        size.height * 0.4737622,
        size.width * 0.8468980,
        size.height * 0.4815700,
        size.width * 0.8162245,
        size.height * 0.4765114);
    path_8.lineTo(size.width * 0.5940327, size.height * 0.4398795);
    path_8.cubicTo(
        size.width * 0.5633551,
        size.height * 0.4348208,
        size.width * 0.5470483,
        size.height * 0.4188111,
        size.width * 0.5576116,
        size.height * 0.4041205);
    path_8.cubicTo(
        size.width * 0.5681748,
        size.height * 0.3894332,
        size.width * 0.6016075,
        size.height * 0.3816254,
        size.width * 0.6322857,
        size.height * 0.3866840);
    path_8.close();

    Paint paint_8_fill = Paint()..style = PaintingStyle.fill;
    paint_8_fill.color = Color(0xff1CB0F6).withOpacity(1.0);
    canvas.drawPath(path_8, paint_8_fill);

    Path path_9 = Path();
    path_9.moveTo(size.width * 0.5806844, size.height * 0.3791075);
    path_9.lineTo(size.width * 0.6478252, size.height * 0.3791075);
    path_9.cubicTo(
        size.width * 0.6663653,
        size.height * 0.3791075,
        size.width * 0.6813946,
        size.height * 0.3863062,
        size.width * 0.6813946,
        size.height * 0.3951824);
    path_9.lineTo(size.width * 0.6813946, size.height * 0.4313485);
    path_9.cubicTo(
        size.width * 0.6813946,
        size.height * 0.4402280,
        size.width * 0.6663653,
        size.height * 0.4474235,
        size.width * 0.6478252,
        size.height * 0.4474235);
    path_9.lineTo(size.width * 0.5806844, size.height * 0.4474235);
    path_9.cubicTo(
        size.width * 0.5621442,
        size.height * 0.4474235,
        size.width * 0.5471143,
        size.height * 0.4402280,
        size.width * 0.5471143,
        size.height * 0.4313485);
    path_9.lineTo(size.width * 0.5471143, size.height * 0.3951824);
    path_9.cubicTo(
        size.width * 0.5471143,
        size.height * 0.3863062,
        size.width * 0.5621442,
        size.height * 0.3791075,
        size.width * 0.5806844,
        size.height * 0.3791075);
    path_9.close();

    Paint paint_9_fill = Paint()..style = PaintingStyle.fill;
    paint_9_fill.color = Color(0xffFFB7E3).withOpacity(1.0);
    canvas.drawPath(path_9, paint_9_fill);

    Path path_10 = Path();
    path_10.moveTo(size.width * 0.6869524, size.height * 0.3946026);
    path_10.lineTo(size.width * 0.6488238, size.height * 0.3933257);
    path_10.cubicTo(
        size.width * 0.6358701,
        size.height * 0.3928925,
        size.width * 0.6246347,
        size.height * 0.3975700,
        size.width * 0.6237286,
        size.height * 0.4037720);
    path_10.cubicTo(
        size.width * 0.6236442,
        size.height * 0.4043485,
        size.width * 0.6236531,
        size.height * 0.4049251,
        size.width * 0.6237544,
        size.height * 0.4055016);
    path_10.lineTo(size.width * 0.6349102, size.height * 0.4689414);
    path_10.cubicTo(
        size.width * 0.6358830,
        size.height * 0.4744756,
        size.width * 0.6451327,
        size.height * 0.4788404,
        size.width * 0.6566993,
        size.height * 0.4792280);
    path_10.lineTo(size.width * 0.7151497, size.height * 0.4811857);
    path_10.cubicTo(
        size.width * 0.7281020,
        size.height * 0.4816189,
        size.width * 0.7393333,
        size.height * 0.4769446,
        size.width * 0.7402449,
        size.height * 0.4707394);
    path_10.cubicTo(
        size.width * 0.7404082,
        size.height * 0.4696124,
        size.width * 0.7402177,
        size.height * 0.4684788,
        size.width * 0.7396735,
        size.height * 0.4673779);
    path_10.lineTo(size.width * 0.7081973, size.height * 0.4032573);
    path_10.cubicTo(
        size.width * 0.7058435,
        size.height * 0.3984560,
        size.width * 0.6972313,
        size.height * 0.3949479,
        size.width * 0.6869524,
        size.height * 0.3946026);
    path_10.close();

    Paint paint_10_fill = Paint()..style = PaintingStyle.fill;
    paint_10_fill.color = Color(0xffFFB7E3).withOpacity(1.0);
    canvas.drawPath(path_10, paint_10_fill);

    Path path_11 = Path();
    path_11.moveTo(size.width * 0.4622571, size.height * 0.1138808);
    path_11.cubicTo(
        size.width * 0.6325952,
        size.height * 0.1138808,
        size.width * 0.7706803,
        size.height * 0.1800007,
        size.width * 0.7706803,
        size.height * 0.2615632);
    path_11.lineTo(size.width * 0.7706803, size.height * 0.2774573);
    path_11.cubicTo(
        size.width * 0.7706803,
        size.height * 0.3590195,
        size.width * 0.6325952,
        size.height * 0.4251401,
        size.width * 0.4622571,
        size.height * 0.4251401);
    path_11.cubicTo(
        size.width * 0.2919184,
        size.height * 0.4251401,
        size.width * 0.1538320,
        size.height * 0.3590195,
        size.width * 0.1538320,
        size.height * 0.2774573);
    path_11.lineTo(size.width * 0.1538320, size.height * 0.2615632);
    path_11.cubicTo(
        size.width * 0.1538320,
        size.height * 0.1800007,
        size.width * 0.2919184,
        size.height * 0.1138808,
        size.width * 0.4622571,
        size.height * 0.1138808);
    path_11.close();

    Paint paint_11_fill = Paint()..style = PaintingStyle.fill;
    paint_11_fill.color = Color(0xffFFB7E3).withOpacity(1.0);
    canvas.drawPath(path_11, paint_11_fill);

    Path path_12 = Path();
    path_12.moveTo(size.width * 0.6843401, size.height * 0.3010241);
    path_12.cubicTo(
        size.width * 0.6915714,
        size.height * 0.3026876,
        size.width * 0.6946122,
        size.height * 0.3068423,
        size.width * 0.6911429,
        size.height * 0.3103036);
    path_12.cubicTo(
        size.width * 0.6288483,
        size.height * 0.3723713,
        size.width * 0.5107782,
        size.height * 0.4106026,
        size.width * 0.3240259,
        size.height * 0.4095081);
    path_12.cubicTo(
        size.width * 0.3072265,
        size.height * 0.4054723,
        size.width * 0.2913082,
        size.height * 0.4007199,
        size.width * 0.2764401,
        size.height * 0.3953388);
    path_12.cubicTo(
        size.width * 0.2785803,
        size.height * 0.3948730,
        size.width * 0.2809837,
        size.height * 0.3946515,
        size.width * 0.2834884,
        size.height * 0.3947427);
    path_12.cubicTo(
        size.width * 0.4827993,
        size.height * 0.4018339,
        size.width * 0.6030694,
        size.height * 0.3659511,
        size.width * 0.6649619,
        size.height * 0.3042798);
    path_12.cubicTo(
        size.width * 0.6684361,
        size.height * 0.3008186,
        size.width * 0.6771122,
        size.height * 0.2993609,
        size.width * 0.6843401,
        size.height * 0.3010241);
    path_12.close();

    Paint paint_12_fill = Paint()..style = PaintingStyle.fill;
    paint_12_fill.color = Color(0xffFF9BD8).withOpacity(1.0);
    canvas.drawPath(path_12, paint_12_fill);

    Path path_13 = Path();
    path_13.moveTo(size.width * 0.3945150, size.height * 0.1524570);
    path_13.cubicTo(
        size.width * 0.5248463,
        size.height * 0.1345622,
        size.width * 0.6607959,
        size.height * 0.1706459,
        size.width * 0.6981701,
        size.height * 0.2330518);
    path_13.cubicTo(
        size.width * 0.7355374,
        size.height * 0.2954580,
        size.width * 0.6601816,
        size.height * 0.3605537,
        size.width * 0.5298503,
        size.height * 0.3784495);
    path_13.cubicTo(
        size.width * 0.3995190,
        size.height * 0.3963453,
        size.width * 0.2635694,
        size.height * 0.3602606,
        size.width * 0.2261973,
        size.height * 0.2978541);
    path_13.cubicTo(
        size.width * 0.1888259,
        size.height * 0.2354482,
        size.width * 0.2641844,
        size.height * 0.1703515,
        size.width * 0.3945150,
        size.height * 0.1524570);
    path_13.close();

    Paint paint_13_fill = Paint()..style = PaintingStyle.fill;
    paint_13_fill.color = Color(0xffE18E70).withOpacity(1.0);
    canvas.drawPath(path_13, paint_13_fill);

    Path path_14 = Path();
    path_14.moveTo(size.width * 0.5250653, size.height * 0.1518072);
    path_14.lineTo(size.width * 0.5245789, size.height * 0.1522446);
    path_14.cubicTo(
        size.width * 0.4864333,
        size.height * 0.1863306,
        size.width * 0.4172810,
        size.height * 0.2378824,
        size.width * 0.2552952,
        size.height * 0.2587345);
    path_14.cubicTo(
        size.width * 0.2436483,
        size.height * 0.2602339,
        size.width * 0.2283531,
        size.height * 0.2604518,
        size.width * 0.2168898,
        size.height * 0.2599430);
    path_14.cubicTo(
        size.width * 0.2216714,
        size.height * 0.2109697,
        size.width * 0.2906286,
        size.height * 0.1667199,
        size.width * 0.3945088,
        size.height * 0.1524570);
    path_14.cubicTo(
        size.width * 0.4387381,
        size.height * 0.1463840,
        size.width * 0.4836143,
        size.height * 0.1465277,
        size.width * 0.5250653,
        size.height * 0.1518072);
    path_14.close();

    Paint paint_14_fill = Paint()..style = PaintingStyle.fill;
    paint_14_fill.color = Color(0xff3D3D3D).withOpacity(1.0);
    canvas.drawPath(path_14, paint_14_fill);

    Path path_15 = Path();
    path_15.moveTo(size.width * 0.3990524, size.height * 0.2848195);
    path_15.cubicTo(
        size.width * 0.4398544,
        size.height * 0.2792173,
        size.width * 0.4634456,
        size.height * 0.2588381,
        size.width * 0.4517463,
        size.height * 0.2393013);
    path_15.cubicTo(
        size.width * 0.4400469,
        size.height * 0.2197645,
        size.width * 0.3974857,
        size.height * 0.2084681,
        size.width * 0.3566844,
        size.height * 0.2140704);
    path_15.cubicTo(
        size.width * 0.3158830,
        size.height * 0.2196723,
        size.width * 0.2922912,
        size.height * 0.2400515,
        size.width * 0.3039912,
        size.height * 0.2595883);
    path_15.cubicTo(
        size.width * 0.3156905,
        size.height * 0.2791254,
        size.width * 0.3582510,
        size.height * 0.2904215,
        size.width * 0.3990524,
        size.height * 0.2848195);
    path_15.close();

    Paint paint_15_fill = Paint()..style = PaintingStyle.fill;
    paint_15_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_15, paint_15_fill);

    Path path_16 = Path();
    path_16.moveTo(size.width * 0.5369939, size.height * 0.2658782);
    path_16.cubicTo(
        size.width * 0.5777952,
        size.height * 0.2602759,
        size.width * 0.6013871,
        size.height * 0.2398967,
        size.width * 0.5896871,
        size.height * 0.2203599);
    path_16.cubicTo(
        size.width * 0.5779878,
        size.height * 0.2008231,
        size.width * 0.5354272,
        size.height * 0.1895267,
        size.width * 0.4946259,
        size.height * 0.1951290);
    path_16.cubicTo(
        size.width * 0.4538238,
        size.height * 0.2007309,
        size.width * 0.4302327,
        size.height * 0.2211101,
        size.width * 0.4419320,
        size.height * 0.2406469);
    path_16.cubicTo(
        size.width * 0.4536320,
        size.height * 0.2601840,
        size.width * 0.4961925,
        size.height * 0.2714801,
        size.width * 0.5369939,
        size.height * 0.2658782);
    path_16.close();

    Paint paint_16_fill = Paint()..style = PaintingStyle.fill;
    paint_16_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_16, paint_16_fill);

    Path path_17 = Path();
    path_17.moveTo(size.width * 0.5335551, size.height * 0.2601410);
    path_17.cubicTo(
        size.width * 0.5677401,
        size.height * 0.2554472,
        size.width * 0.5875061,
        size.height * 0.2383726,
        size.width * 0.5777041,
        size.height * 0.2220039);
    path_17.cubicTo(
        size.width * 0.5679014,
        size.height * 0.2056352,
        size.width * 0.5322429,
        size.height * 0.1961707,
        size.width * 0.4980571,
        size.height * 0.2008645);
    path_17.cubicTo(
        size.width * 0.4638721,
        size.height * 0.2055580,
        size.width * 0.4441061,
        size.height * 0.2226326,
        size.width * 0.4539088,
        size.height * 0.2390013);
    path_17.cubicTo(
        size.width * 0.4637109,
        size.height * 0.2553700,
        size.width * 0.4993701,
        size.height * 0.2648345,
        size.width * 0.5335551,
        size.height * 0.2601410);
    path_17.close();

    Paint paint_17_fill = Paint()..style = PaintingStyle.fill;
    paint_17_fill.color = Color(0xff3D3D3D).withOpacity(1.0);
    canvas.drawPath(path_17, paint_17_fill);

    Path path_18 = Path();
    path_18.moveTo(size.width * 0.5136156, size.height * 0.2312479);
    path_18.lineTo(size.width * 0.5180850, size.height * 0.2296997);
    path_18.cubicTo(
        size.width * 0.5359313,
        size.height * 0.2235863,
        size.width * 0.5502415,
        size.height * 0.2192143,
        size.width * 0.5391102,
        size.height * 0.2118785);
    path_18.cubicTo(
        size.width * 0.5273612,
        size.height * 0.2041352,
        size.width * 0.5022878,
        size.height * 0.2032674,
        size.width * 0.4831075,
        size.height * 0.2099401);
    path_18.cubicTo(
        size.width * 0.4639265,
        size.height * 0.2166127,
        size.width * 0.4579020,
        size.height * 0.2282990,
        size.width * 0.4696517,
        size.height * 0.2360423);
    path_18.cubicTo(
        size.width * 0.4810993,
        size.height * 0.2435873,
        size.width * 0.4951599,
        size.height * 0.2377261,
        size.width * 0.5136156,
        size.height * 0.2312479);
    path_18.lineTo(size.width * 0.5136156, size.height * 0.2312479);
    path_18.close();

    Paint paint_18_fill = Paint()..style = PaintingStyle.fill;
    paint_18_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_18, paint_18_fill);

    Path path_19 = Path();
    path_19.moveTo(size.width * 0.5513361, size.height * 0.2452873);
    path_19.cubicTo(
        size.width * 0.5601578,
        size.height * 0.2440759,
        size.width * 0.5652585,
        size.height * 0.2396697,
        size.width * 0.5627293,
        size.height * 0.2354456);
    path_19.cubicTo(
        size.width * 0.5601993,
        size.height * 0.2312215,
        size.width * 0.5509973,
        size.height * 0.2287788,
        size.width * 0.5421748,
        size.height * 0.2299902);
    path_19.cubicTo(
        size.width * 0.5333531,
        size.height * 0.2312013,
        size.width * 0.5282524,
        size.height * 0.2356078,
        size.width * 0.5307816,
        size.height * 0.2398319);
    path_19.cubicTo(
        size.width * 0.5333116,
        size.height * 0.2440560,
        size.width * 0.5425136,
        size.height * 0.2464987,
        size.width * 0.5513361,
        size.height * 0.2452873);
    path_19.close();

    Paint paint_19_fill = Paint()..style = PaintingStyle.fill;
    paint_19_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_19, paint_19_fill);

    Path path_20 = Path();
    path_20.moveTo(size.width * 0.3956143, size.height * 0.2790827);
    path_20.cubicTo(
        size.width * 0.4297993,
        size.height * 0.2743893,
        size.width * 0.4495653,
        size.height * 0.2573147,
        size.width * 0.4397626,
        size.height * 0.2409459);
    path_20.cubicTo(
        size.width * 0.4299605,
        size.height * 0.2245772,
        size.width * 0.3943014,
        size.height * 0.2151127,
        size.width * 0.3601163,
        size.height * 0.2198065);
    path_20.cubicTo(
        size.width * 0.3259313,
        size.height * 0.2245000,
        size.width * 0.3061653,
        size.height * 0.2415746,
        size.width * 0.3159673,
        size.height * 0.2579433);
    path_20.cubicTo(
        size.width * 0.3257701,
        size.height * 0.2743121,
        size.width * 0.3614293,
        size.height * 0.2837765,
        size.width * 0.3956143,
        size.height * 0.2790827);
    path_20.close();

    Paint paint_20_fill = Paint()..style = PaintingStyle.fill;
    paint_20_fill.color = Color(0xff3D3D3D).withOpacity(1.0);
    canvas.drawPath(path_20, paint_20_fill);

    Path path_21 = Path();
    path_21.moveTo(size.width * 0.3742000, size.height * 0.2506993);
    path_21.lineTo(size.width * 0.3801177, size.height * 0.2486410);
    path_21.cubicTo(
        size.width * 0.3979633,
        size.height * 0.2425277,
        size.width * 0.4122735,
        size.height * 0.2381557,
        size.width * 0.4011429,
        size.height * 0.2308199);
    path_21.cubicTo(
        size.width * 0.3893939,
        size.height * 0.2230765,
        size.width * 0.3643197,
        size.height * 0.2222088,
        size.width * 0.3451395,
        size.height * 0.2288814);
    path_21.cubicTo(
        size.width * 0.3259592,
        size.height * 0.2355541,
        size.width * 0.3199347,
        size.height * 0.2472404,
        size.width * 0.3316837,
        size.height * 0.2549837);
    path_21.cubicTo(
        size.width * 0.3428306,
        size.height * 0.2623300,
        size.width * 0.3564537,
        size.height * 0.2569664,
        size.width * 0.3742000,
        size.height * 0.2506993);
    path_21.lineTo(size.width * 0.3742000, size.height * 0.2506993);
    path_21.close();

    Paint paint_21_fill = Paint()..style = PaintingStyle.fill;
    paint_21_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_21, paint_21_fill);

    Path path_22 = Path();
    path_22.moveTo(size.width * 0.4133680, size.height * 0.2642283);
    path_22.cubicTo(
        size.width * 0.4221898,
        size.height * 0.2630173,
        size.width * 0.4272912,
        size.height * 0.2586107,
        size.width * 0.4247612,
        size.height * 0.2543866);
    path_22.cubicTo(
        size.width * 0.4222320,
        size.height * 0.2501625,
        size.width * 0.4130293,
        size.height * 0.2477202,
        size.width * 0.4042075,
        size.height * 0.2489313);
    path_22.cubicTo(
        size.width * 0.3953857,
        size.height * 0.2501427,
        size.width * 0.3902844,
        size.height * 0.2545489,
        size.width * 0.3928143,
        size.height * 0.2587730);
    path_22.cubicTo(
        size.width * 0.3953442,
        size.height * 0.2629974,
        size.width * 0.4045463,
        size.height * 0.2654397,
        size.width * 0.4133680,
        size.height * 0.2642283);
    path_22.close();

    Paint paint_22_fill = Paint()..style = PaintingStyle.fill;
    paint_22_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_22, paint_22_fill);

    Path path_23 = Path();
    path_23.moveTo(size.width * 0.6027014, size.height * 0.2259160);
    path_23.cubicTo(
        size.width * 0.5511510,
        size.height * 0.2341476,
        size.width * 0.5139116,
        size.height * 0.2509609,
        size.width * 0.4923129,
        size.height * 0.2756430);
    path_23.cubicTo(
        size.width * 0.4868041,
        size.height * 0.2819375,
        size.width * 0.4929959,
        size.height * 0.2891782,
        size.width * 0.5061415,
        size.height * 0.2918156);
    path_23.cubicTo(
        size.width * 0.5192864,
        size.height * 0.2944531,
        size.width * 0.5344088,
        size.height * 0.2914886,
        size.width * 0.5399170,
        size.height * 0.2851941);
    path_23.cubicTo(
        size.width * 0.5558204,
        size.height * 0.2670205,
        size.width * 0.5817483,
        size.height * 0.2553140,
        size.width * 0.6190299,
        size.height * 0.2493609);
    path_23.cubicTo(
        size.width * 0.6325503,
        size.height * 0.2472020,
        size.width * 0.6398565,
        size.height * 0.2402036,
        size.width * 0.6353476,
        size.height * 0.2337293);
    path_23.cubicTo(
        size.width * 0.6308388,
        size.height * 0.2272550,
        size.width * 0.6162224,
        size.height * 0.2237570,
        size.width * 0.6027014,
        size.height * 0.2259160);
    path_23.close();

    Paint paint_23_fill = Paint()..style = PaintingStyle.fill;
    paint_23_fill.color = Color(0xffE18E70).withOpacity(1.0);
    canvas.drawPath(path_23, paint_23_fill);

    Path path_24 = Path();
    path_24.moveTo(size.width * 0.4727170, size.height * 0.2823736);
    path_24.cubicTo(
        size.width * 0.4840789,
        size.height * 0.2808137,
        size.width * 0.4906259,
        size.height * 0.2751020,
        size.width * 0.4873408,
        size.height * 0.2696163);
    path_24.cubicTo(
        size.width * 0.4840558,
        size.height * 0.2641306,
        size.width * 0.4557143,
        size.height * 0.2539811,
        size.width * 0.4557143,
        size.height * 0.2539811);
    path_24.lineTo(size.width * 0.4546184, size.height * 0.2553912);
    path_24.cubicTo(
        size.width * 0.4515905,
        size.height * 0.2593853,
        size.width * 0.4435095,
        size.height * 0.2707772,
        size.width * 0.4461973,
        size.height * 0.2752655);
    path_24.cubicTo(
        size.width * 0.4494823,
        size.height * 0.2807511,
        size.width * 0.4613558,
        size.height * 0.2839336,
        size.width * 0.4727170,
        size.height * 0.2823736);
    path_24.close();

    Paint paint_24_fill = Paint()..style = PaintingStyle.fill;
    paint_24_fill.color = Color(0xff9E4D31).withOpacity(1.0);
    canvas.drawPath(path_24, paint_24_fill);

    Path path_25 = Path();
    path_25.moveTo(size.width * 0.4485224, size.height * 0.2915453);
    path_25.cubicTo(
        size.width * 0.4579422,
        size.height * 0.2934479,
        size.width * 0.4678776,
        size.height * 0.2936818,
        size.width * 0.4783286,
        size.height * 0.2922466);
    path_25.cubicTo(
        size.width * 0.4889707,
        size.height * 0.2907853,
        size.width * 0.4973442,
        size.height * 0.2879788,
        size.width * 0.5034490,
        size.height * 0.2838264);
    path_25.cubicTo(
        size.width * 0.5058510,
        size.height * 0.2821932,
        size.width * 0.5105626,
        size.height * 0.2818013,
        size.width * 0.5139735,
        size.height * 0.2829508);
    path_25.cubicTo(
        size.width * 0.5153830,
        size.height * 0.2834261,
        size.width * 0.5164109,
        size.height * 0.2841179,
        size.width * 0.5168857,
        size.height * 0.2849114);
    path_25.lineTo(size.width * 0.5352007, size.height * 0.3154945);
    path_25.cubicTo(
        size.width * 0.5414293,
        size.height * 0.3258958,
        size.width * 0.5288701,
        size.height * 0.3367427,
        size.width * 0.5071490,
        size.height * 0.3397264);
    path_25.cubicTo(
        size.width * 0.4854286,
        size.height * 0.3427101,
        size.width * 0.4627714,
        size.height * 0.3366938,
        size.width * 0.4565429,
        size.height * 0.3262932);
    path_25.lineTo(size.width * 0.4383265, size.height * 0.2958746);
    path_25.cubicTo(
        size.width * 0.4371762,
        size.height * 0.2939544,
        size.width * 0.4394946,
        size.height * 0.2919515,
        size.width * 0.4435048,
        size.height * 0.2914010);
    path_25.cubicTo(
        size.width * 0.4451626,
        size.height * 0.2911733,
        size.width * 0.4469333,
        size.height * 0.2912241,
        size.width * 0.4485224,
        size.height * 0.2915453);
    path_25.close();

    Paint paint_25_fill = Paint()..style = PaintingStyle.fill;
    paint_25_fill.color = Color(0xff881B1B).withOpacity(1.0);
    canvas.drawPath(path_25, paint_25_fill);

    Path path_26 = Path();
    path_26.moveTo(size.width * 0.4939782, size.height * 0.3177078);
    path_26.cubicTo(
        size.width * 0.5099946,
        size.height * 0.3155088,
        size.width * 0.5265367,
        size.height * 0.3183567,
        size.width * 0.5361925,
        size.height * 0.3242837);
    path_26.cubicTo(
        size.width * 0.5335612,
        size.height * 0.3314300,
        size.width * 0.5227088,
        size.height * 0.3375928,
        size.width * 0.5071639,
        size.height * 0.3397264);
    path_26.cubicTo(
        size.width * 0.4916184,
        size.height * 0.3418599,
        size.width * 0.4755939,
        size.height * 0.3393876,
        size.width * 0.4654551,
        size.height * 0.3339967);
    path_26.cubicTo(
        size.width * 0.4670850,
        size.height * 0.3265179,
        size.width * 0.4779619,
        size.height * 0.3199072,
        size.width * 0.4939782,
        size.height * 0.3177078);
    path_26.close();

    Paint paint_26_fill = Paint()..style = PaintingStyle.fill;
    paint_26_fill.color = Color(0xffB12E2E).withOpacity(1.0);
    canvas.drawPath(path_26, paint_26_fill);

    Path path_27 = Path();
    path_27.moveTo(size.width * 0.1758878, size.height * 0.2195124);
    path_27.lineTo(size.width * 0.1792721, size.height * 0.2195433);
    path_27.cubicTo(
        size.width * 0.2328646,
        size.height * 0.2202560,
        size.width * 0.2498415,
        size.height * 0.2046984,
        size.width * 0.2692721,
        size.height * 0.1931430);
    path_27.cubicTo(
        size.width * 0.2822259,
        size.height * 0.1854394,
        size.width * 0.3033946,
        size.height * 0.1890319,
        size.width * 0.3327776,
        size.height * 0.2039199);
    path_27.lineTo(size.width * 0.2810252, size.height * 0.2320805);
    path_27.lineTo(size.width * 0.1932109, size.height * 0.2330349);
    path_27.lineTo(size.width * 0.1868612, size.height * 0.2316736);
    path_27.cubicTo(
        size.width * 0.1491762,
        size.height * 0.2234293,
        size.width * 0.1455184,
        size.height * 0.2193756,
        size.width * 0.1758878,
        size.height * 0.2195124);
    path_27.lineTo(size.width * 0.1758878, size.height * 0.2195124);
    path_27.close();

    Paint paint_27_fill = Paint()..style = PaintingStyle.fill;
    paint_27_fill.color = Color(0xff3D3D3D).withOpacity(1.0);
    canvas.drawPath(path_27, paint_27_fill);

    Path path_28 = Path();
    path_28.moveTo(size.width * 0.1397143, size.height * 0.2165199);
    path_28.lineTo(size.width * 0.2942660, size.height * 0.2288450);
    path_28.cubicTo(
        size.width * 0.3024544,
        size.height * 0.2295414,
        size.width * 0.3068612,
        size.height * 0.2338635,
        size.width * 0.3029673,
        size.height * 0.2373811);
    path_28.cubicTo(
        size.width * 0.2842619,
        size.height * 0.2542821,
        size.width * 0.2443891,
        size.height * 0.2636612,
        size.width * 0.2040252,
        size.height * 0.2602293);
    path_28.cubicTo(
        size.width * 0.1636619,
        size.height * 0.2567974,
        size.width * 0.1328463,
        size.height * 0.2414081,
        size.width * 0.1272558,
        size.height * 0.2224414);
    path_28.cubicTo(
        size.width * 0.1260925,
        size.height * 0.2184938,
        size.width * 0.1315252,
        size.height * 0.2158238,
        size.width * 0.1397143,
        size.height * 0.2165199);
    path_28.close();

    Paint paint_28_fill = Paint()..style = PaintingStyle.fill;
    paint_28_fill.color = Color(0xff3D3D3D).withOpacity(1.0);
    canvas.drawPath(path_28, paint_28_fill);

    Path path_29 = Path();
    path_29.moveTo(size.width * 0.4846184, size.height * 0.3927622);
    path_29.lineTo(size.width * 0.6624626, size.height * 0.4380423);
    path_29.cubicTo(
        size.width * 0.6911088,
        size.height * 0.4453355,
        size.width * 0.7019864,
        size.height * 0.4623681,
        size.width * 0.6867551,
        size.height * 0.4760847);
    path_29.cubicTo(
        size.width * 0.6715211,
        size.height * 0.4898046,
        size.width * 0.6359497,
        size.height * 0.4950098,
        size.width * 0.6073020,
        size.height * 0.4877166);
    path_29.lineTo(size.width * 0.4294571, size.height * 0.4424397);
    path_29.cubicTo(
        size.width * 0.4008095,
        size.height * 0.4351466,
        size.width * 0.3899340,
        size.height * 0.4181107,
        size.width * 0.4051660,
        size.height * 0.4043941);
    path_29.cubicTo(
        size.width * 0.4203986,
        size.height * 0.3906775,
        size.width * 0.4559701,
        size.height * 0.3854691,
        size.width * 0.4846184,
        size.height * 0.3927622);
    path_29.close();

    Paint paint_29_fill = Paint()..style = PaintingStyle.fill;
    paint_29_fill.color = Color(0xff1CB0F6).withOpacity(1.0);
    canvas.drawPath(path_29, paint_29_fill);

    Path path_30 = Path();
    path_30.moveTo(size.width * 0.5878925, size.height * 0.4500098);
    path_30.lineTo(size.width * 0.6718177, size.height * 0.3804072);
    path_30.cubicTo(
        size.width * 0.6880408,
        size.height * 0.3669511,
        size.width * 0.7239728,
        size.height * 0.3623420,
        size.width * 0.7520680,
        size.height * 0.3701107);
    path_30.cubicTo(
        size.width * 0.7801701,
        size.height * 0.3778762,
        size.width * 0.7897959,
        size.height * 0.3950814,
        size.width * 0.7735714,
        size.height * 0.4085342);
    path_30.lineTo(size.width * 0.6896463, size.height * 0.4781401);
    path_30.cubicTo(
        size.width * 0.6734238,
        size.height * 0.4915928,
        size.width * 0.6374939,
        size.height * 0.4962052,
        size.width * 0.6093952,
        size.height * 0.4884365);
    path_30.cubicTo(
        size.width * 0.5812966,
        size.height * 0.4806678,
        size.width * 0.5716694,
        size.height * 0.4634625,
        size.width * 0.5878925,
        size.height * 0.4500098);
    path_30.close();

    Paint paint_30_fill = Paint()..style = PaintingStyle.fill;
    paint_30_fill.color = Color(0xff1CB0F6).withOpacity(1.0);
    canvas.drawPath(path_30, paint_30_fill);

    Path path_31 = Path();
    path_31.moveTo(size.width * 0.7817687, size.height * 0.4593485);
    path_31.lineTo(size.width * 0.7271224, size.height * 0.3833550);
    path_31.cubicTo(
        size.width * 0.7165578,
        size.height * 0.3686645,
        size.width * 0.7328639,
        size.height * 0.3526580,
        size.width * 0.7635442,
        size.height * 0.3475993);
    path_31.cubicTo(
        size.width * 0.7942177,
        size.height * 0.3425407,
        size.width * 0.8276531,
        size.height * 0.3503485,
        size.width * 0.8382177,
        size.height * 0.3650391);
    path_31.lineTo(size.width * 0.8928639, size.height * 0.4410326);
    path_31.cubicTo(
        size.width * 0.9034286,
        size.height * 0.4557199,
        size.width * 0.8871224,
        size.height * 0.4717296,
        size.width * 0.8564422,
        size.height * 0.4767883);
    path_31.cubicTo(
        size.width * 0.8257619,
        size.height * 0.4818469,
        size.width * 0.7923333,
        size.height * 0.4740391,
        size.width * 0.7817687,
        size.height * 0.4593485);
    path_31.close();

    Paint paint_31_fill = Paint()..style = PaintingStyle.fill;
    paint_31_fill.color = Color(0xff1CB0F6).withOpacity(1.0);
    canvas.drawPath(path_31, paint_31_fill);

    Path path_32 = Path();
    path_32.moveTo(size.width * 0.7527143, size.height * 0.2906990);
    path_32.cubicTo(
        size.width * 0.8013810,
        size.height * 0.2906990,
        size.width * 0.8408367,
        size.height * 0.3095902,
        size.width * 0.8408367,
        size.height * 0.3328925);
    path_32.cubicTo(
        size.width * 0.8408367,
        size.height * 0.3561987,
        size.width * 0.8013810,
        size.height * 0.3750879,
        size.width * 0.7527143,
        size.height * 0.3750879);
    path_32.cubicTo(
        size.width * 0.7040476,
        size.height * 0.3750879,
        size.width * 0.6645939,
        size.height * 0.3561987,
        size.width * 0.6645939,
        size.height * 0.3328925);
    path_32.cubicTo(
        size.width * 0.6645939,
        size.height * 0.3095902,
        size.width * 0.7040476,
        size.height * 0.2906990,
        size.width * 0.7527143,
        size.height * 0.2906990);
    path_32.close();

    Paint paint_32_fill = Paint()..style = PaintingStyle.fill;
    paint_32_fill.color = Color(0xffE18E70).withOpacity(1.0);
    canvas.drawPath(path_32, paint_32_fill);

    Path path_33 = Path();
    path_33.moveTo(size.width * 0.8030748, size.height * 0.3027544);
    path_33.cubicTo(
        size.width * 0.8517415,
        size.height * 0.3027544,
        size.width * 0.8911905,
        size.height * 0.3216459,
        size.width * 0.8911905,
        size.height * 0.3449511);
    path_33.cubicTo(
        size.width * 0.8911905,
        size.height * 0.3682541,
        size.width * 0.8517415,
        size.height * 0.3871433,
        size.width * 0.8030748,
        size.height * 0.3871433);
    path_33.cubicTo(
        size.width * 0.7544014,
        size.height * 0.3871433,
        size.width * 0.7149524,
        size.height * 0.3682541,
        size.width * 0.7149524,
        size.height * 0.3449511);
    path_33.cubicTo(
        size.width * 0.7149524,
        size.height * 0.3216459,
        size.width * 0.7544014,
        size.height * 0.3027544,
        size.width * 0.8030748,
        size.height * 0.3027544);
    path_33.close();

    Paint paint_33_fill = Paint()..style = PaintingStyle.fill;
    paint_33_fill.color = Color(0xffE18E70).withOpacity(1.0);
    canvas.drawPath(path_33, paint_33_fill);

    Paint paint_34_fill = Paint()..style = PaintingStyle.fill;
    paint_34_fill.color = Color(0xffAFAFAF).withOpacity(1.0);
    canvas.drawRRect(
        RRect.fromRectAndCorners(
            Rect.fromLTWH(size.width * 0.3033320, size.height * 0.8795993,
                size.width * 0.4249646, size.height * 0.02665453),
            bottomRight: Radius.circular(size.width * 0.02783313),
            bottomLeft: Radius.circular(size.width * 0.02783313),
            topLeft: Radius.circular(size.width * 0.02783313),
            topRight: Radius.circular(size.width * 0.02783313)),
        paint_34_fill);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
