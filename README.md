# svg_paint

example to handle user clicks over an custom painter
## Created by Omar Mouki

## Getting Started
1.  First use this [tool](https://fluttershapemaker.com/) to convert the svg to custom painter
    this tool will convert the svg paths to a custom painter paths
    copy the generated code to the project
    video [demo](https://www.youtube.com/watch?v=U51GvNzBRSc&ab_channel=RetroPortalStudio) 

 2.	Use the flutter plugin [touchable](https://pub.dev/packages/touchable)
    this plugin allows the user to interact with the custom painter paths 
    ### how to use it
    -	Wrap the custom painter widget with a CanvasTouchDetector widget 
    ```dart
    body: Center(
          child: CanvasTouchDetector(
        builder: (context) => CustomPaint(
          size: Size(
              500,
              (500 * 2.0884353741496597)
                  .toDouble()), 
          painter: RPSCustomPainter(context, color: color),
        ),
      )),
    ```
    -	Replace the ```canvas.drawPath``` method with ```myCanvas.drawPath``` method to detect user clicks
    ```dart
     Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Colors.green.withOpacity(1.0);
    // new code
    myCanvas.drawPath(path_0, paint_0_fill, onTapDown: (details) {
      print('leg clicked');
    }, onPanDown: (tapdetail) {
      // print("orange circle swiped");
    });
    // canvas.drawPath(path_0, paint_0_fill); // old generated code
    ``` 
